import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plot

def mydata():
    mydata = [[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9],
              [10, 11, 12],
              [13, 14, 15],
              [16, 17, 18],
              [19, 20, 21],
              [22, 23, 24],
              [25, 26, 27],
              [28, 29, 30],
              [31, 32, 33],
              [34, 35, 36]]
    return np.array(mydata)

def eval_classifier(actual,predicted):
    length = len(np.unique(actual))

    conf_matrix = np.zeros((length,length))

    for index in range(length):
        i = int(actual[index])
        j = int(predicted[index])
        conf_matrix[i][j] += 1

    conf_indices = np.indices((length,length))

    rows = np.ravel(conf_indices[0])
    cols = np.ravel(conf_indices[1])
    values = np.ravel(conf_matrix)*200
    #c = ['black' if -0.5 < i < 0.5 else 'r' for i in y])
    plot.show()

def main():
    time_start = time.time()

    ### Data injest
    print("[INFO] Importing data...")
    #ar_eval_input = pd.read_csv('training_data_partial.csv', delimiter=",").values
    ar_eval_input = mydata()
    time_data_import = time.time()
    print("[INFO] Time taken to import data: {:0.1f}s".format(time_data_import-time_start))

    ### Manipulate data here for classifier

    num_folds = 3
    num_rows_per_fold = int(ar_eval_input.shape[0]/num_folds)
    num_row_1st_test_set = 0

    for fold in range(num_folds):
        print("[INFO] Iteration number: ",fold)

        ar_copy = ar_eval_input
        num_row_last_test_set = num_row_1st_test_set + num_rows_per_fold

        ar_test_set = ar_copy[num_row_1st_test_set:num_row_last_test_set,:]
        ar_training_set = np.delete(ar_copy,np.s_[num_row_1st_test_set:num_row_last_test_set],axis=0)

        ### Classifier training here

        ### Classifier testing here for evaluation - will need to capture all eval metric
        ### from each loop and then do an average outside for loop
        num_row_1st_test_set += num_rows_per_fold

    eval_input = pd.read_csv('eval_input.csv', delimiter=" ").values
    eval_classifier(eval_input[:,1],eval_input[:,2])
    ###
    ### Run classifier on assignment test set

    ### Summarize eval information

main()


























