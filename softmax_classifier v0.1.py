

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse
import scipy
import time

def oneHotIt(Y):
    m = Y.shape[0]
    OHX = scipy.sparse.csr_matrix((np.ones(m), (Y, np.array(range(m)))))
    OHX = np.array(OHX.todense()).T
    return OHX

def getLoss(w,x,y,lam):
    m = x.shape[0] #First we get the number of training examples
    y_mat = oneHotIt(y.ravel()) #Next we convert the integer class coding into a one-hot representation
    scores = np.dot(x,w) #Then we compute raw class scores given our input and current weights
    prob = softmax(scores) #Next we perform a softmax on these scores to get their probabilities
    loss = (-1 / m) * np.sum(y_mat * np.log(prob)) + (lam/2)*np.sum(w*w) #We then find the loss of the probabilities
    grad = (-1 / m) * np.dot(x.T,(y_mat - prob)) + lam*w #And compute the gradient for that loss
    return loss,grad

def softmax(z):
    z -= np.max(z)
    sm = (np.exp(z).T / np.sum(np.exp(z),axis=1)).T
    return sm

def getProbsAndPreds(someX,w):
    probs = softmax(np.dot(someX,w))
    preds = np.argmax(probs,axis=1)
    return probs,preds

def getProbsAndPreds(someX,w):
    probs = softmax(np.dot(someX,w))
    preds = np.argmax(probs,axis=1)
    return probs,preds

def getAccuracy(someX,someY,someZ,w):
    prob,prede = getProbsAndPreds(someX,w)

    #a = someX[:,0]
    #b = someX[:,1]
    #e = np.stack((a,b,someY,prede),axis=1)

    print(someZ.shape,someY.shape)
    e = np.stack((someZ,someY,prede),axis=1)
    print(e)
    np.savetxt("eval_input.csv",e,delimiter=',',fmt='%s %0.2f %0.2f')

    accuracy = sum(prede == someY)/(float(len(someY)))
    return accuracy

data_load_time_start = time.clock()

training_data = pd.read_csv('training_data.csv', delimiter =',').values

data_load_time = time.clock() - data_load_time_start
print("Time taken to load data: ", data_load_time)

data_trans_time_start = time.clock()

x = training_data[0:15000,2:]
y = training_data[0:15000,1:2]
z = training_data[0:15000,0]
test_x = training_data[15001:,2:]
test_y = training_data[15001:,1:2]
test_z = training_data[15001:,0]

x = x.astype(float)
y = y.astype(float)
test_x = test_x.astype(float)
test_y = test_y.astype(float)

data_trans_time = time.clock() - data_trans_time_start
print("Time taken to transform data: ", data_trans_time)

w = np.ones([x.shape[1],len(np.unique(y))])

lam = 1
iterations = 30
learningRate = 1e-1
losses = []

learn_time = time.clock()

for i in range(0,iterations):
    loss,grad = getLoss(w,x,y,lam)
    losses.append(loss)
    w = w - (learningRate * grad)

learn_time = time.clock()-learn_time
print("Time taken to learn parameters: ", learn_time)

predict = time.clock()

probs, preds = getProbsAndPreds(x,w)

predict = time.clock()- predict
print("Time taken to predict test classes: ", predict)

print(preds)
print ('Test Accuracy: ', getAccuracy(test_x,test_y.ravel(),test_z,w))
print("Total time taken: ", time.clock()-data_load_time_start)

plt.plot(losses)
plt.show()